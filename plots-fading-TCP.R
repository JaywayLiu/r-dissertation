

#this part needs the data from running the first 9 lines of new-processData-revise-sinr.R
cl <- rainbow(7)
tcpScale =  0.368368345145189 



dd$Scaled.TCP.Prediction <- dd$Shannon.Capacitya * tcpScale
dd$Prediction.Error.for.Scaled.TCP <- dd$Scaled.TCP.Prediction - dd$TCP.Goodput
dd$Percentage.Prediction.Error.for.Scaled.TCP <- dd$Prediction.Error.for.Scaled.TCP / dd$TCP.Goodput *100
outd = dd[order(dd$Distance),]
pp =outd[which(outd$Distance >=2000),]

plot.new()

cl <- rainbow(7)
plot(0, 0,xlim=c(0,20), ylim=c(0, 15), xlab="SNR (dB)",ylab="Throughput (Mbps)",main="Best fitting of SNR based predictions \nto TCP measurements", type="n")
lines(pp$SINR.dB.,pp$TCP.Goodput,col = cl[1],type = 'b')
lines(pp$SINR.dB.,pp$Scaled.TCP.Prediction, pch =2, col = cl[3],type = 'b')
legend("topleft", c("TCP Goodput", "SNR based Prediction"), pch = c(1,2), col=c(cl[1], cl[3]))

########################################

setwd("/home/jianwel/Documents/R-files")
dd = read.csv("allResults-fading-tcp.txt", sep =",")
cl <- rainbow(7)
tcpScale =  0.1005



dd$Scaled.TCP.Prediction <- dd$Shannon.Capacitya * tcpScale
dd$Prediction.Error.for.Scaled.TCP <- dd$Scaled.TCP.Prediction - dd$TCP.Goodput
dd$Percentage.Prediction.Error.for.Scaled.TCP <- dd$Prediction.Error.for.Scaled.TCP / dd$TCP.Goodput *100
outd = dd[order(dd$Distance),]
pp =outd[which(outd$Distance >500),]

plot.new()

cl <- rainbow(7)
plot(0, 0,xlim=c(0,20), ylim=c(0, 6), xlab="SNR (dB)",ylab="Throughput (Mbps)",main="Best fitting of SNR based predictions \nto TCP measurements", type="n")
lines(pp$SINR.dB.,pp$TCP.Goodput,col = cl[1],type = 'b')
lines(pp$SINR.dB.,pp$Scaled.TCP.Prediction, pch =2, col = cl[3],type = 'b')
legend("topleft", c("TCP Goodput", "SNR based Prediction"), pch = c(1,2), col=c(cl[1], cl[3]))


